#include <fstream>
#include <iostream>

int main ( int argc, char *argv[] )
{
    if ( argc != 6 )
        std::cout<<"usage: "<< argv[0] <<" [-c|-d] -i <filename> -o <filename>" << std::endl;
    else {
        std::ifstream the_file ( argv[3] );
        if ( !the_file.is_open() )
            std::cout<<"Could not open file\n";
        else {
            char x;
            while ( the_file.get ( x ) )
                std::cout<< x;
        }
    }
}