# Extra Credit Lab #
##### Due November 30, 2017 #####
The goal of this extra credit lab is to reinforce the concept of trees, but also introduce you to actual algorithms that do real things. You will be implementing lossless text compression using Huffman Coding. There are many resources on how this algorithm works. Check the **Helpful Links** section down below. 

You will need to show that your program can take in a text file, compress it, and then store that compressed file. You will then need to be able to decompress the file using your program as well. These file inputs and outputs will be assigned on the command line using flags. This will be a physical demo; anyone that wishes to demonstrate this must have a working C++ Huffman Coding program. You will also be given an example sentence that you will need to compress by hand to show that you understand how Huffman coding works. 

#### usage: ####

To compress *index.txt* and store it in *compressed.cmp*

``` huffman -c -i input.txt -o compressed.cmp ``` 


To decompress *compressed.cmp* and store it in *decompressed.txt*

`huffman -d -i compressed.cmp -o decompressed.txt`

This extra credit lab will be worth one full lab. This assignment is an all or nothing assignment, you either complete it, or you don't. There will be no partial credit. This is supposed to be challenging.

###Helpful links:###

* [Explanation of Huffman Coding and Lossless Text Compression](https://www.youtube.com/watch?v=JsTptu56GM8)

* [Huffman Coding Wikipedia Page](https://en.wikipedia.org/wiki/Huffman_coding)

* [Youtube search for huffman coding](https://www.youtube.com/results?search_query=huffman+coding)

* [How to setup command-line arguments](http://www.cplusplus.com/articles/DEN36Up4/)